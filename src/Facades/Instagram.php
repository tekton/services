<?php namespace Tekton\Services\Facades;

class Instagram extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'services.instagram'; }
}
