<?php namespace Tekton\Services\Facades;

class Youtube extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'services.youtube'; }
}
